package qb.test.jira.TestSample;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)				
@CucumberOptions(features="Features/LGA_Test.feature",glue={"StepDefinition"},format={"json:target/Destination/cucumber123.json"})	
public class Runner {

}

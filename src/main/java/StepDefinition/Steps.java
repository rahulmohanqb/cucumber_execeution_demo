package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class Steps {

	public static WebDriver d;

	@Given("^Open the Firefox and launch the application$")
	public void open_the_Firefox_and_launch_the_application() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "E:\\Automation Workspace\\Seleium Drivers/geckodriver.exe");
		d = new FirefoxDriver();
		d.get("https://compressimage.toolur.com/");
		d.quit();
	}

	@Given("^Open the browser and launch the LGA application$")
	public void open_the_browser_and_launch_the_LGA_application() throws Throwable {
		try {
			System.setProperty("webdriver.gecko.driver", "E:\\Automation Workspace\\Seleium Drivers/geckodriver.exe");
			d = new FirefoxDriver();
			d.get("https://www.lgamerica.com/");
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println("Element not found");
		}

	}

	@Then("^Click on the Sign In Button$")
	public void click_on_the_Sign_In_Button() throws Throwable {
		try {
			d.findElement(By.xpath("//a[@class='btn btn-success gtmSignIn']")).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Element not found");
		}

	}

	@Then("^wait for page to load completely$")
	public void wait_for_page_to_load_completely() throws Throwable {
		try {
			JavascriptExecutor js = (JavascriptExecutor) d;
			js.executeScript("return document.readyState").toString().equals("complete");
		} catch (Exception e) {
			System.out.println("Element not found");
		}

	}

	@When("^test the presence of one element in the app page$")
	public void test_the_presence_of_one_element_in_the_app_page() throws Throwable {
		try {
			if (d.findElement(By.xpath("//input[@name='username']")) != null) {
				d.findElement(By.xpath("//input[@name='username']")).sendKeys("TestUser123");
				d.quit();
				//Assert.assertEquals(true, false);
			}
		} catch (Exception e) {
			System.out.println("Element not found");
		}

	}
}
